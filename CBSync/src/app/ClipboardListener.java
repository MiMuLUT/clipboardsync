package app;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ClipboardListener extends Thread implements ClipboardOwner {
	private ObjectOutputStream oos = null;
	private String host;
	private int port;
	private Clipboard clipboard;
	private Socket socket;

	public ClipboardListener(String host, int port, Clipboard clipboard) {
		this.host = host;
		this.port = port;
		this.clipboard = clipboard;
	}

	public void run() {
		takeClipboardOwnership();
	}

	// This takes ownership of the clipboard to get informed when it loses ownership
	// over it.
	// Losing ownership means that some other application has changed the
	// clipboard's contents.
	private void takeClipboardOwnership() {
		try {
			Transferable trans = clipboard.getContents(this);
			clipboard.setContents(trans, this);
		} catch (IllegalStateException e) {
			try {
				// Sleep when the clipboard was already in use
				Thread.sleep(10);
				takeClipboardOwnership();
			} catch (InterruptedException ex) {
				Thread.currentThread().interrupt();
				takeClipboardOwnership();
			}
		}
	}

	private void createConnection() {
		try {
			socket = new Socket(host, port);
			oos = new ObjectOutputStream(socket.getOutputStream());
		} catch (UnknownHostException e) {
			System.out.println("Unknown host.");
			System.exit(1);
		} catch (ConnectException e) {
			System.out.println("Connection was refused.");
			// Retry connection until user exits or connection is established
			createConnection();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		System.out.println("Clipboard changed.");
		takeClipboardOwnership();
		if (oos == null) {
			createConnection();
		}

		if (clipboard.isDataFlavorAvailable(DataFlavor.stringFlavor)) {
			try {
				Object string = clipboard.getData(DataFlavor.stringFlavor);
				oos.writeObject((String) string);
				System.out.println("Sent text.");
			} catch (SocketException e) {
				System.out.println("Disconnected.");
				System.exit(1);
			} catch (UnsupportedFlavorException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			} finally {
				try {
					if (oos != null) {				
						oos.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				oos = null;
			}
		}
	}

	public void cleanUp() {
		try {
			if (oos != null) {
				oos.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
