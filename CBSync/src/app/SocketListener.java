package app;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class SocketListener extends Thread {
	private ClipboardListener cbListener;
	private Clipboard clipboard;
	private ServerSocket serverSocket;
	private ObjectInputStream ois;

	public SocketListener(int localPort, ClipboardListener cbListener, Clipboard clipboard) {
		this.cbListener = cbListener;
		this.clipboard = clipboard;
		try {
			serverSocket = new ServerSocket(localPort);
			System.out.println("Listening on port: " + serverSocket.getLocalPort());
		} catch (IOException e) {
			System.out.println("Could not listen on port: " + localPort);
			System.exit(1);
		}
	}

	public void run() {
		try {
			while (true) {
				if (serverSocket.isClosed()) {
					return;
				}
				Socket newSocket = serverSocket.accept();
				ois = new ObjectInputStream(newSocket.getInputStream());
				Object data = ois.readObject();
				
				if (data instanceof String) {
					TransferableString stringTrans = new TransferableString((String) data);
					clipboard.setContents((Transferable) stringTrans, cbListener);
					System.out.println("Received text.");
				} else {
					System.out.println("Received invalid data.");
				}
				ois.close();
			}
		} catch (SocketException e) {
			// Socket closed
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void cleanUp() {
		try {
			if (ois != null) {
				ois.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
