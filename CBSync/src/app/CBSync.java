package app;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.util.Scanner;

public class CBSync {
	private ClipboardListener cbListener;
	private SocketListener socketListener;

	public static void main(String args[]) {
		checkArgs(args);
		new CBSync(args);
	}

	private static void printUsage() {
		System.out.println("CBSync: syncronise clipboard between two computers.");
		System.out.println("Only text supported.");
		System.out.println("Usage: java -jar CBSync.jar <localport> <remote host> <remote port>");
		System.exit(1);
	}

	private static void checkArgs(String args[]) {
		if (args.length != 3) {
			printUsage();
		}

		try {
			int one = Integer.parseInt(args[0]);
			int two = Integer.parseInt(args[2]);
			if (one > 65535 || two > 65535 || one < 0 || two < 0) {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			System.out.println("Invalid port number.");
			printUsage();
		}
	}

	private CBSync(String args[]) {
		int localPort = Integer.parseInt(args[0]);
		String host = args[1];
		int port = Integer.parseInt(args[2]);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		System.out.println("Type 'exit' to stop the program.");

		cbListener = new ClipboardListener(host, port, clipboard);
		socketListener = new SocketListener(localPort, cbListener, clipboard);
		cbListener.start();
		socketListener.start();

		Scanner read = new Scanner(System.in);
		while (true) {
			String input = read.nextLine();
			if (input.equals("exit")) {
				read.close();
				cleanAndExit();
			} else {
				System.out.println("Invalid input.");
			}
		}
	}

	public void cleanAndExit() {
		socketListener.cleanUp();
		cbListener.cleanUp();
		System.exit(0);
	}
}
